<?php
//  message received on facebook platform 
class messageFB {
  public $PSID; //The PSID of the user that triggered the webhook event.
  public $timestamp;
  public $messageContent;
  public $mid; //message id 
  function __construct($message) {
    $this->$PSID = $message->sender->id; 
    $this->$timestamp = $message->timestamp; 
    $this->$messageContent = $message->message->text; 
    $this->$mid = $message->message->mid;
  }
  
  function get_id() {
    return $this->PSID;
  }
  function get_timestamp() {
    return $this->timestamp;
  }
  function get_platform() {
    return "Facebook Messenger";
  }
  function get_message_details() {
    return array( $this->$messageContent, $this->$timestamp);
  }
}
?>


<!-- json example of received message -->
<!-- {
  "sender":{
    "id":"<PSID>"
  },
  "recipient":{
    "id":"<PAGE_ID>"
  },
  "timestamp":1458692752478,
  "message":{
    "mid":"mid.1457764197618:41d102a3e1ae206a38",
    "text":"hello, world!",
    "quick_reply": {
      "payload": "<DEVELOPER_DEFINED_PAYLOAD>"
    }
  }
}   -->

