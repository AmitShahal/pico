<?php
//  message received on facebook platform 
class messageTwilio {
  public $sid; 
  public $from;
  public $timestamp;
  public $messageContent;
  function __construct($message) {
    $this->$sid = $message->sid; 
    $this->$from = $message->$from;
    $this->$timestamp = strtotime($message->date_sent); 
    $this->$messageContent = $message->body; 
}
function get_id() {
    return $this->sid;
  }
  function get_timestamp() {
    return $this->timestamp;
  }
  function get_platform() {
    return "twilio sms";
  }
  function get_message_details() {
    return array($this->$messageContent, $this->$timestamp);
  }
}
?>


<!-- json example of received message -->
<!-- {
  "account_sid": "ACXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
  "api_version": "2010-04-01",
  "body": "Join Earth's mightiest heroes. Like Kevin Bacon.",
  "date_created": "Thu, 30 Jul 2015 20:12:31 +0000",
  "date_sent": "Thu, 30 Jul 2015 20:12:33 +0000",
  "date_updated": "Thu, 30 Jul 2015 20:12:33 +0000",
  "direction": "outbound-api",
  "error_code": null,
  "error_message": null,
  "from": "+15017122661",
  "messaging_service_sid": "MGXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
  "num_media": "0",
  "num_segments": "1",
  "price": null,
  "price_unit": null,
  "sid": "SMXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
  "status": "sent",
  "subresource_uris": {
    "media": "/2010-04-01/Accounts/ACXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX/Messages/SMXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX/Media.json"
  },
  "to": "+15558675310",
  "uri": "/2010-04-01/Accounts/ACXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX/Messages/SMXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX.json"
}  -->