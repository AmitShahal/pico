<?php
//  message received on facebook platform 
class user {
  public $id; 
  public $dateUserAdded;
  public $platform;
  public $conversation; // array of messages

  function __construct($message) {
    $this->$id = $message->get_id(); 
    $this->$dateUserAdded = $message->get_timestamp(); 
    $this->$platform = $message->get_platform(); //  twilio or fb messenger
    array_push($this->$conversation, $message->get_message_details()); //message details -> {messageContent, timestamp}
}
 public function AddMessage($message){
    array_push($this->$conversation, $message->get_message_details());
  }

  public static function newUserFromDb(string $json) {
    $data = json_decode($json);
    return new static(
      $data['id'],
      $data['dateUserAdded'], 
      $data['platform'], 
      $data['conversation']);
}
}
?>
